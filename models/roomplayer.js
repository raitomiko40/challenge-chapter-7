'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class RoomPlayer extends Model {
    
    static associate(models) {
      // define association here
    }
  }
  RoomPlayer.init({
    roomId: {
     type: DataTypes.INTEGER,
     allowNull: false,
    },
    playerId: {
      type: DataTypes.INTEGER,
      allowNull: false,
     },
  }, {
    sequelize,
    modelName: 'RoomPlayer',
  });
  return RoomPlayer;
};