const express = require('express')

const passport = require("./lib/passport");
const app = express()
const port = 3000

const authenticateRoute = require("./routers/authentication")
const roomRoute = require('./routers/room')
const gameplayRoute = require('./routers/gameplay')

app.use(express.urlencoded()) 
app.use(passport.initialize());
app.use(authenticateRoute)
app.use(roomRoute)
app.use(gameplayRoute)

app.get('/', (req, res) => {
  res.send('Jumat, 19 May 2023 19.00!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})