'use strict';
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Rooms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      winnerId: {
        allowNull: true,
        type: Sequelize.INTEGER,
        references: {
          model : "Users",
          key : "id",
        }
      },
      status: {
        allowNull: false,
        type: Sequelize.ENUM(['OPEN', "IN_PROGRESS", "CLOSED"]),
        defaultValue: "OPEN"
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Rooms');
  }
};