const express = require("express");
const router = express.Router();
const passport = require("../lib/passport");
const roomControllers = require("../controllers/room");

router.post(
    '/room',
    passport.authenticate("jwt", { session : false }),
    roomControllers.createRoom 
)

module.exports = router