const { RoomPlayer , Room} = require ("../models")

exports.join = async (req, res) => {
    const room = await Room.findOne({
        where: {
            id: req.body.roomId
        },
        include: RoomPlayer
    })
    if (!room) {
        res.status(404).send("Room id tidak ditemukan!")
        return
    }

    const find = room.RoomPlayers.find((item) => item.playerId === req.user.id)
    if (find) {
        res.status(500).send('User sudah join di room ini')
        return
    }

    if (room.RoomPlayers.length === 2){
    res.status(500).send('Room penuh')
    return
    }

    const roomPlayer = await RoomPlayer.create({
        roomId: req.body.roomId,
        playerId: req.user.id
    })
    res.send(roomPlayer)
}