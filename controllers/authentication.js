const jwt = require("jsonwebtoken");
const {User} = require("../models");

exports.Register = async(req, res) =>{
     await User.create(req.body)
     res.send('berhasil registrasi')
}

exports.login = async (req,res) => {
    try {
        const user = await User.findOne({
          where: {
            username: req.body.username,
            password: req.body.password,
          },
        });
    
        if (user) {
          const payload = { uid: user.id };
          const secret = "Secret";
    
          const token = jwt.sign(payload, secret);
          res.json({
            accesstoken: token,
          });
        } else {
          res.status(401).send("Invalid login!");
        }
      } catch (err) {
        console.log(err);
        res.status(500).send(err.message);
      }
}