const { Room } = require ("../models")

exports.createRoom = async (req, res) => {
    const room = await Room.create(req.body)
    res.send('Berhasil Membuat Room dengan Id : ' + room.id)
}